const sayHelloWorld = () => {
  const helloText = 'Hello World';
  console.log('Will say hello now: ');
  console.log(`${helloText}`);
};

const zaWarudo = async () => {
  sayHello();
};

zaWarudo();